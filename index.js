const express = require("express");
const bp = require("body-parser");

const router = require("./routes");

// init express instance
const app = express();

// init body parser instances
app.use(bp.json({ limit: "100mb" }));
app.use(
  bp.urlencoded({
    extended: true,
    limit: "100mb",
  })
);

// init API routes
app.use(router);

const PORT = 3000;

app.listen(PORT, () => {
  console.log("listenig to http:://localhost:" + PORT);
});
